 <!-- Page Heading -->
 <?php $this->view('alert'); ?>
 <h1 class="h3 mb-2 text-gray-800">Laporan transaksi</h1>
 <!-- DataTales Example -->
 <div class="card shadow mb-4">
     <div class="card-header py-3">
         <div class="card-header py-3">
             <a class="btn btn-lg  btn-info active" id="detail-barang" data-toggle="modal" data-target="#modal-detail">
                 <i class="fa fa-print" aria-hidden="true"> Print</i>
             </a>
         </div>
     </div>
     <div class="card-body">
         <div class="table-responsive">
             <table class="table table-bordered" id="table1" width="100%" cellspacing="0">
                 <thead>
                     <tr>
                         <th>#</th>
                         <th>Customer</th>
                         <th>Date</th>
                         <th>Total</th>

                     </tr>
                 </thead>
                 <tfoot>
                 </tfoot>
                 <tbody>
                     <?php
                        $total = 0;
                        $no = 1;
                        foreach ($detail as $user) {
                            // $totalharga = (int) $user->total;
                            // $total += $totalharga;
                        ?>
                         <tr>
                             <td><?= $no++ ?></td>
                             <td><?= $user->id == null ? "Umum"  : $user->cusnama ?></td>

                             <td><?= date('d F Y', strtotime($user->pencre)); ?></td>
                             <td><?= format_rupiah($user->final_harga) ?></td>

                         </tr>
                     <?php
                        } ?>
                     <tr>
                         <!-- <td colspan="4">Total Semua</td>
                         <td><?= format_rupiah($total) ?></td> -->
                     </tr>
                 </tbody>
             </table>
             <!-- <div class="card-footer">
                 <a href="<?= site_url('transaksi/detail_print/') . $user->id_penjualan ?>" target="_blank" class="btn btn-info active" role="button" aria-pressed="true"> <i class="fas fa-print"></i>Print</a>
                 <a href="<?= site_url('app/cetak_nota/') . $user->id_penjualan ?>" target="_blank" class="btn btn-primary active" role="button" aria-pressed="true"> <i class="fas fa-print"></i> Struk</a>
             </div> -->
         </div>
     </div>
 </div>
 <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title font-weight-bold text-primary text-center" id="exampleModalLabel">
                     Cetak Laporan
                 </h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">

                 <form method="post" class="form" action="<?= base_url('transaksi/ppp') ?>">
                     <tr>
                         <td align="center" width="5%">
                             <div class="form-group"> Dari Tanggal :
                                 <input type="date" class="form-control" name="tgla" required>
                             </div>
                         </td>
                     </tr>
                     <tr>
                         <td align="center" width="5%">
                             <div class="form-group"> Sampai Tanggal :
                                 <input type="date" class="form-control" name="tglb" required>
                             </div>
                         </td>
                     </tr>
                     <tr>
                         <td></td>
                         <td>
                             <input type="submit" class="fbtn btn-primary btn-sm" value="Cetak">
                         </td>
                     </tr>
                 </form>
             </div>
             <div class="modal-footer">
                 <form method="post" class="form" action="<?= base_url('transaksi/laporan') ?>">
                     <button class="btn btn-success" type="submit" id="linkHapus" href="">Cetak Semua</button>
                 </form>
             </div>
         </div>
     </div>