<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POS | Print | Detail</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>

<body>
    <center>

        <h2>Laporan Transaksi</h2>
        <h4>App POS</h4>
        <h6><?= date('d F ', strtotime($a)); ?> - <?= date('d F Y', strtotime($b)); ?></h6>

    </center>
    <br />
    <table class="table table-bordered" id="table1" width="70%" cellspacing="0">
        <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $total = 0;
            $no = 1;
            foreach ($details as $user) {
                $totalharga = (int) $user->final_harga;
                $total += $totalharga;

            ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $user->id == null ? "Umum"  : $user->cusnama ?></td>
                    <td><?= date('d F Y', strtotime($user->date)); ?></td>

                    <td><?= format_rupiah ($user->final_harga) ?></td>
                </tr>
            <?php
            } ?>
            <tr>
                <td colspan="3">Total Semua</td>
                <td><?= format_rupiah ($total) ?></td>
            </tr>
        </tbody>
    </table>

</body>

</html>